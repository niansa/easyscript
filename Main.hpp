#pragma once

namespace Game {
template<class Manager>
class Main;
}
#ifndef MAIN_HPP
#define MAIN_HPP
#include "Namespace.hpp"
#include "FriendlyApp.hpp"

#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/EngineDefs.h>
#include <Urho3D/Core/Object.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Scene/Scene.h>



namespace Game {
template<class Manager>
class Main final : public FriendlyApp {
    friend SceneManager;

    URHO3D_OBJECT(Main, Application);

    Manager levelManager;

public:
    // Initialization
    explicit Main(Context* context) : FriendlyApp(context), levelManager(this) {
        engineParameters_[EP_RESOURCE_PREFIX_PATHS] = "Project";
    }

    virtual void Start() override {
        levelManager.Start();
    }
};
}
#endif // MAIN_HPP
