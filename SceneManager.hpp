#pragma once

namespace Game {
struct SceneManager;
}
#ifndef SCENEMANAGER_HPP
#define SCENEMANAGER_HPP
#include "Namespace.hpp"
#include "FriendlyApp.hpp"

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>



namespace Game {
struct SceneManager {
    static constexpr eastl::string_view spawner_prefix = "Spawn_";

    FriendlyApp *app;
    Urho3D::Scene *scene = nullptr;
    eastl::string objsDir = "Objects/";

    SceneManager(FriendlyApp *app);

    Urho3D::Node *LoadNode(const eastl::string& name);
    void LoadScripted(Node *scriptedNode);
    void PrepareScene();
    void FinalizeScene();
    void LoadScene(const eastl::string& filename);
    void AsyncLoadScene(const eastl::string& filename);
};
}
#endif // SCENEMANAGER_HPP
