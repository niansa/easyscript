#pragma once

namespace Game {
class FriendlyApp;
}
#ifndef FRIENDLYAPP_HPP
#define FRIENDLYAPP_HPP
#include "Namespace.hpp"

#include <Urho3D/Engine/Application.h>



namespace Game {
struct SceneManager;

class FriendlyApp : public Application {
    friend SceneManager;

public:
    using Application::Application;

    auto& GetEngineParameters() {
        return engineParameters_;
    }
};
}
#endif // FRIENDLYAPP_HPP
