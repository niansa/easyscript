#include "SceneManager.hpp"
#include "generated/loadSpawnerIncs.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Resource/XMLFile.h>



namespace Game {
SceneManager::SceneManager(FriendlyApp *app) : app(app) {
#   include "generated/componentRegisterers.cppi"
}

Node *SceneManager::LoadNode(const eastl::string& name) {
    auto* cache = app->GetSubsystem<ResourceCache>();
    auto file = cache->GetResource<XMLFile>(name);

    if (file) {
        auto node = scene->CreateChild();
        node->LoadXML(file->GetRoot());
        return node;
    } else {
        //TODO
        abort();
    }
}

void SceneManager::LoadScripted(Node *scriptedNode) {
    auto objectName = scriptedNode->GetVar("Object").GetString();
    if (scriptedNode->HasTag("Spawner")) {
        // Load node
        auto node = LoadNode(objsDir+objectName+".xml")->GetChild(objectName);
        // Change node ownership
        auto holderNode = node->GetParent();
        scriptedNode->AddChild(node);
        holderNode->Remove();
        // Add script component
        node->CreateComponent(objectName);
    } else if (scriptedNode->HasTag("Scripted")) {
        // Add script component
        scriptedNode->CreateComponent(objectName);
    }
}

void SceneManager::PrepareScene() {
    if (scene) {
        scene->Clear();
    } else {
        scene = new Scene(app->context_);
    }

    scene->CreateComponent<Octree>();
}
void SceneManager::FinalizeScene() {
    for (const auto& node : scene->GetChildren(true)) {
        LoadScripted(node);
    }
}

void SceneManager::LoadScene(const eastl::string& filename) {
    PrepareScene();

    auto* cache = app->GetSubsystem<ResourceCache>();
    auto file = cache->GetResource<XMLFile>(filename);

    scene->LoadXML(file->GetRoot());

    FinalizeScene();
}
void SceneManager::AsyncLoadScene(const eastl::string& filename) {
    PrepareScene();

    auto* cache = app->GetSubsystem<ResourceCache>();
    auto file = cache->GetFile(filename);

    scene->LoadAsyncXML(file);
}
}
